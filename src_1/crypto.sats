(* ****** ****** *)
//
// CS520 Final Project
//
(* ****** ****** *)

(*
** Author: Shuwen Sun
** Start time: November, 2015
*)

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
staload
STDLIB = "libc/SATS/stdlib.sats"
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

datatype list(a:t@ype) =
  | cons of (a, list a)
  | nil of ()

typedef ilist = list(int)

(* ****** ****** *)
//
// compute m^e mod n
//
fun
modpow
  (n:int, e:int, m:int): int

 (* ****** ****** *)
//
// compute the Jacobi symbol (a,n)
//
fun
jacobi(a:int, n:int): int

(* ****** ****** *)
//
// check if a is a Euler witness for n
//
fun
solvayPrime(a:int, n:int) : bool

(* ****** ****** *)
//
// Test if n is probably prime
// TODO: isPrime to probablePrime
//
fun
isPrime(n:int): bool

(* ****** ****** *)
//
// find a random prime between 3 and n-1
//
fun
randPrime(n:int): int

(* ****** ****** *)
//
// computes gcd(a,b)
//
fun
{m,n:int | m > 0; n > 0}
gcd
(
  m:int(m), n:int(n)
): [r:nat | 1 <= r; r <= min(m, n)] int r

(* ****** ****** *)
//
// find a random exponent between 3 and n-1, s.t. gcd(x, phi) = 1
//
fun
randExponent(phi:int, n:int): int

(* ****** ****** *)
//
// compute the n^~1 mod n by extend eulidian method
//
fun
inverse(n:int, modulus:int): int

(* ****** ****** *)
//
// encrypt the message m using public exponent and modulus, c = m^e mod n
//
fun
encrypt
  (n:int, e:int, m:int): int

(* ****** ****** *)
//
// decrypt the crypted c using private exponent and modulus, m = c^d mod n
//
fun
decrypt
  (n:int, d:int, c:int): int


(* ****** ****** *)
////
// encrypt a list
//
fun
encryptlist
  (n:int, e:int, msglist: ilist): ilist

(* ****** ****** *)

fun
decryptlist
  (d:int, n:int, klist:ilist): ilist

(* ****** ****** *)

(* ****** ****** *)

(* https://www.openssl.org/docs/manmaster/crypto/RSA_public_encrypt.html *)
////
fun
public_encrypt(n:int, e:int, msg:int):int

(* ****** ****** *)

fun
private_decrypt(n:int, e:int, msg:int):int

(* ****** ****** *)
