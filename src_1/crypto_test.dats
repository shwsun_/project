(* ****** ****** *)
//
// Implement crypto test
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)s

staload
UN = "prelude/SATS/unsafe.sats"
//
staload "./crypto.sats"

(* ****** ****** *)
(*
** first prime factor: p
** second prime factor: q
** generate modulus: n = p*q
** generate totient: phi
** choose public exponent: e
** choose private exponent: d
**
** PublicKey as: (e, n)
** PrivateKey as: (d, n)
**
*)
implement
main0 (argc, argv) =
{
//
val out = stdout_ref
//
val message = "Welcome to RSA demonstration!"
val () = fprintln! (out, message)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "first prime factor: p = ", p)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "second prime factor: q = " q)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "generate modulus: n = p*q = ", n)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "choose public exponent: e = ", e)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "choose private exponent: d = ", d)
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, " PublicKey as: (e, n) = (", e, ",", n, ")")
val ((*void*)) = fprint_newline (out)
//
val () = fprintln! (out, "PrivateKey as: (d, n) = (", d, ",", n, ")")
val ((*void*)) = fprint_newline (out)
//

}

(* ****** ****** *)
