(* ****** ****** *)
//
// CS520 Final Project
//
(* ****** ****** *)

(*
** Author: Shuwen Sun
** Start time: November, 2015
*)

(* ****** ****** *)
//
staload
UN = "prelude/SATS/unsafe.sats"
//
staload
STRING = "libc/SATS/string.sats"
//
#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
//
staload "libc/SATS/math.sats"
//
(* ****** ****** *)

staload
"./crypto.sats"
//
//assume ilist = list(int)
//
//#define nil list_nil
//#define :: list_cons
//#define cons list_cons

(* ****** ****** *)
//
// might need another way to prevent overlow
//
implement
modpow(n,e,m) =
  pow(m, e) mod n

(* ****** ****** *)
// presume that n is odd
implement
jacobi(a, n) = let
//
val res =
(
if a = 0 then
  if n = 1
    then 1 else 0 // end of [if]
else
if a = 2 then
  if (n mod 8) = 3 then -1
  else if (n mod 8) = 5 then -1 else 1 // end of [if]
else if a >= n then jacobi(a mod n, n)
  else if (a mod 2) = 0 then jacobi(2, n)*jacobi(a/2, n)
    else (
let
  val j = jacobi(n,a)
in
  if (a mod 4 =3) && (n mod 4 = 3) then ~j else j
end )
)
in
  res
end // end of [jacobi]

(* ****** ****** *)

implement
solvayPrime(a,n) = let
//
val res =
if jacobi(a,n) = 0 then false
  else if jacobi(a,n) = ~1 then
    if modpow(a, (n-1)/2, n) = jacobi(a,n)-1 then true else false
    else false
in
  res
end // end of [solvayPrime]

(* ****** ****** *)
//
// modify to probablePrime with a accuracy range
//
implement
isPrime(n) = let
//
val nq = int_of sqrt(n-0.5)
//
fun aux(n:int, i:int): bool =
if i <= nq then
  if n mod i = 0 then false else aux(n, i+1)
else true // end of [aux]
//
in
  if n >= 2 then aux(n, 2) else false
end // end of [isPrime]

(* ****** ****** *)

%{
int
randPrime(int n) {
	int prime = rand() % n;
	n += n % 2; /* n needs to be even so modulo wrapping preserves oddness */
	prime += 1 - prime % 2;
	while(1) {
		if(probablePrime(prime, ACCURACY)) return prime;
		prime = (prime + 2) % n;
	}
}
%} // end of [randPrime]

(* ****** ****** *)

implement
randPrime(n) = "ext#randPrime"

(* ****** ****** *)

implement
gcd(m, n) =
(
if m > n then gcd (m - n, n)
  else if m < n then gcd (m, n - m)
  else m
) // end of [gcd]

(* ****** ****** *)

implement
randExponent(phi, n) =let
val e = $UN.cast2int($STDLIB.random()) mod 100
//
val res  = ()
//
fun
aux (phi:int, n:int, e:int): int =
(
if gcd(e, phi) = 1 then res = e
  else if e <= 2 then e = 3
  else aux(phi, n, (e+1)%n)
)
in
  aux(phi, n, e)
end

%{
int
inverse(int n, int modulus) {
	int a = n, b = modulus;
	int x = 0, y = 1, x0 = 1, y0 = 0, q, temp;
	while(b != 0) {
		q = a / b;
		temp = a % b;
		a = b;
		b = temp;
		temp = x; x = x0 - q * x; x0 = temp;
		temp = y; y = y0 - q * y; y0 = temp;
	}
	if(x0 < 0) x0 += modulus;
	return x0;
}
%} // end of [inverse in C]

(* ****** ****** *)
implement
inverse(n, modulus) = "ext#inverse"

(* ****** ****** *)

implement
encrypt(n, e, m) =  modpow(n,e,m)

(* ****** ****** *)

implement
decrypt(n, d, c) = modpow(n, d, c)

(* ****** ****** *)
(*
implement
encryptlist(n, e, msg_list) = let
//
fun
aux(n:int, e:int, xs:ilist): ilist =
(
case xs of
| nil() => nil()
| m::xss => cons(pow(m,e) mod n, aux(n, e, xss))
)
in
  aux(n, e, msg_list)
end
(* ****** ****** *)

implement
decryptlist(d, n, klist) = let
//
fun
aux (d:int, n:int, xs:ilist): ilist =
(
case+ xs of
| nil() => nil()
| c::xss => cons(, aux(d, n, xss))
)
in
  aux(d, n, klist)
end

(* ****** ****** *)
////
implement
encryptstr(n, e, msgstr):
implement
decryptstr(d, n, kstr):
*)

(* ****** ****** *)


(* ****** ****** *)
