
# Final Project for BU CS520

The project intend to do RSA encrypt and decrypt in ATS, which will basically generate the public key and private key.

Use the public key to encrypt a msg, and decrypt the msg using private key.


To compile
---
$ make test
or test any specific function, respectively

Files
---
datatypes and functions are in crypto.[s|d]ats
tests are in crypto_test.dats

Progress in ATS
---
Basic function to call is almost finished.

TEST data is generated using C


## Explanation

Due to time limit and not efficiently coding in ATS, I can't finish the rest of the project, which is basically how to test the functionality of the project. But I did write the functions and used ATS style.  When I run this example in C, it shows like

Got first prime factor, p = 3593 ...
Got second prime factor, q = 2677 ...
Got modulus, n = pq = 9618461 ...
Got totient, phi = 9612192 ...
Chose public exponent, e = 239
Public key is (239, 9618461) ...
Calculated private exponent, d = 3096815
Private key is (3096815, 9618461) ...
Opening file "text.txt" for reading
File "text.txt" read successfully, 8043 bytes read. Encoding byte stream in chunks of 3 bytes ...

Then it shows the encrypted stuff(using the public key pair), and it can use the private key pair to get the real message.

I just feel like I should explain more about my project to make you easier to judge my work.
