symintr eq
symintr gt
symintr gte
symintr lt
symintr lte
symintr compare
symintr empty
//symintr min 
//symintr max

symintr append
symintr prepend
symintr concat
symintr head
symintr tail
symintr take
symintr drop
symintr map 
symintr zip
symintr filter
symintr foldl
symintr foldr
symintr foreach

symintr fst
symintr snd

symintr bind

symintr len

symintr show

overload show with print_bool
overload show with print_string
overload show with print_char
overload show with print_int
overload show with print_float
overload show with print_double